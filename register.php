<?php
session_start();
if (isset($_POST['submit'])) {


    $name = $_POST["name"];
    $user = $_POST["user"];
    $pass = $_POST["pass"];
    $mobileNumber = $_POST["mobileNumber"];

    header('location: index.php');
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }

        .topnav {
            overflow: hidden;
            background-color: #333;
        }

        .topnav a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Create a right-aligned (split) link inside the navigation bar */
        .topnav a.split {
            float: right;
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>

<body>

    <div class="topnav">
        <a href="">Mini Blog</a>
        <a href="index.php" class="split" onclick='redirectLogin()'>Login</a>
    </div>

    <div style="padding-left:16px">
        <div class="container mt-5">
            <div class="card container">
                <div class="card-body">
                    <h3>Register</h3>
                </div>
                <hr>
                <form method="post" action="">
                    <div class="form-group">
                        <label for="usr">Username:</label>
                        <input type="text" class="form-control" id="user" name="user">
                    </div>
                    <div class="form-group">
                        <label for="usr">Email:</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pass" name="pass">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Confirm Password:</label>
                        <input type="password" class="form-control" id="re-pass" name="pass">
                    </div>
                    <button type="submit" name='submit' class="btn btn-primary mb-2" onclick="redirectRegister()">Register</button>
                </form>
            </div>
        </div>
    </div>

</body>

</html>
<script>
    function redirectLogin() {
        let base_url = window.location.origin;
        window.location.replace(`${base_url}/mini-blog`);
    }
</script>